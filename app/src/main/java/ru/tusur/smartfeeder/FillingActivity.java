package ru.tusur.smartfeeder;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

public class FillingActivity extends AppCompatActivity {

    private BarChart barChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filling);

        barChart = findViewById(R.id.barChart);

        List<BarEntry> filling = new ArrayList<>();
        filling.add(new BarEntry(1, 110));
        filling.add(new BarEntry(2, 98));
        filling.add(new BarEntry(3, 140));
        filling.add(new BarEntry(4, 120));
        filling.add(new BarEntry(5, 150));
        filling.add(new BarEntry(6, 90));
        filling.add(new BarEntry(7, 20));
        filling.add(new BarEntry(8, 18));

        BarDataSet dataSet = new BarDataSet(filling, "Filling");
        dataSet.setColors(ColorTemplate.MATERIAL_COLORS);
        dataSet.setValueTextColor(Color.BLACK);
        dataSet.setValueTextSize(16f);

        BarData barData = new BarData(dataSet);

        barChart.setFitBars(true);
        barChart.setData(barData);

        Description description = new Description();
        description.setText("Filling your smart feeder");
        barChart.setDescription(description);
        barChart.animateY(2000);


    }
}